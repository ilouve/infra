module "ecr" {
  source                 = "./ecr"
  name                   = module.service.name
  principals_full_access = ["arn:aws:iam::032481722713:root"]
}

//resource "aws_route53_record" "record" {
//  zone_id = data.terraform_remote_state.vpc.outputs.route53_zone.zone_id
//  name    = "api.flyadam.com.br"
//  type    = "A"
//
//  alias {
//    name                   = data.terraform_remote_state.vpc.outputs.alb.dns_name
//    zone_id                = data.terraform_remote_state.vpc.outputs.alb.zone_id
//    evaluate_target_health = true
//  }
//}

module "service" {
  source = "./service"
  name = "ms-discovery"
  cluster = data.terraform_remote_state.vpc.outputs.cluster
  task_definitions = [module.task-definition]

  health_check = {
    timeout = 10
    path = "/health"
  }
}

module "task-definition" {
  source            = "./task-definition"
  container_name    = "ms-discovery"
  container_image   = "032481722713.dkr.ecr.us-east-1.amazonaws.com/ms-discovery:latest"
  container_memory  = 512
  container_cpu     = 512

  port_mappings = [
    {
      containerPort = 10000
      hostPort      = 0
      protocol      = "tcp"
    }
  ]

  environment = [
    {
      name  = "JAVA_MIN_HEAP_FREE_RATIO"
      value = "10"
    },
    {
      name  = "JAVA_MAX_HEAP_FREE_RATIO"
      value = "70"
    },
    {
      name  = "JAVA_COMPRESSED_CLASS_SPACE_SIZE"
      value = "64m"
    },
    {
      name  = "JAVA_RESERVED_CODE_CACHE_SIZE"
      value = "64m"
    },
    {
      name  = "JAVA_MAX_META_SPACE_SIZE"
      value = "256m"
    },
    {
      name  = "JAVA_XMS"
      value = "256m"
    },
    {
      name  = "JAVA_XMX"
      value = "450m"
    }
  ]

  log_configuration =  {
    logDriver =  "awslogs"
    options =  {
      awslogs-region = "us-east-1"
      awslogs-group = "ms-discovery"
      awslogs-stream-prefix = "frwk"
    }
    secretOptions = []
  }
}
