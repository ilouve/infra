locals {
  container_definitions = join(",", [for t in var.task_definitions : replace(t.json, "\\\"", "\"")])
  raw = jsondecode(local.container_definitions)
}

resource "aws_ecs_task_definition" "this" {
  family                   = "${var.name}-task"
  //execution_role_arn     = aws_iam_role.ecs_task_execution_role.arn
  container_definitions    = local.container_definitions

  tags = {
    Terraform = true
  }
}

resource "aws_ecs_service" "this" {
  name            = "${var.name}-service"
  cluster         = var.cluster.id
  task_definition = aws_ecs_task_definition.this.arn
  desired_count   = var.replicas
  deployment_maximum_percent = 200
  deployment_minimum_healthy_percent = 100

  service_registries {
    registry_arn    = aws_service_discovery_service.this.arn
    container_name  = local.raw[0].name
    container_port  = element(local.raw[0].portMappings.*.containerPort, 0)
  }

  lifecycle {
    create_before_destroy = true
  }

  #depends_on = [aws_iam_role_policy_attachment.ecs_task_execution_role]
}

# SERVICE DISCOVERY

resource "aws_service_discovery_service" "this" {
  name = var.name

  dns_config {
    namespace_id = var.cluster.vpc.service_discovery_namespace.id
    routing_policy = "MULTIVALUE"

    dns_records {
      ttl  = 10
      type = "SRV"
    }
  }

  health_check_custom_config {
    failure_threshold = 5
  }
}


resource "aws_cloudwatch_log_group" "log_group" {
  name              = var.name
  retention_in_days = 1
}
