<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">

    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>2.1.5.RELEASE</version>
        <relativePath />
    </parent>

    <groupId>com.framework.ms</groupId>
    <artifactId>frwk-ms-parent</artifactId>
    <version>0.0.1-SNAPSHOT</version>

    <packaging>pom</packaging>

    <properties>
        <java.version>1.8</java.version>
        <maven.compiler.source>${java.version}</maven.compiler.source>
        <maven.compiler.target>${java.version}</maven.compiler.target>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>

        <frwk-ms-parent.version>0.0.1-SNAPSHOT</frwk-ms-parent.version>
        <frwk-ms-commons.version>0.0.1-SNAPSHOT</frwk-ms-commons.version>

        <swagger-annotations.version>1.5.22</swagger-annotations.version>
        <swagger-codegen-maven-plugin.version>2.4.5</swagger-codegen-maven-plugin.version>
        <aws-java-sdk-ssm.version>1.11.415</aws-java-sdk-ssm.version>
        <commons-io.version>2.6</commons-io.version>
        <springfox-swagger2.version>2.9.2</springfox-swagger2.version>
        <spring-boot.version>2.1.5.RELEASE</spring-boot.version>
        <spring-cloud.version>Greenwich.RELEASE</spring-cloud.version>
    </properties>

    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-dependencies</artifactId>
                <version>${spring-boot.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
            <dependency>
                <groupId>org.springframework.cloud</groupId>
                <artifactId>spring-cloud-dependencies</artifactId>
                <version>${spring-cloud.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>

            <!-- Others dependencies -->
            <dependency>
                <groupId>com.amazonaws</groupId>
                <artifactId>aws-java-sdk-ssm</artifactId>
                <version>${aws-java-sdk-ssm.version}</version>
            </dependency>
            <dependency>
                <groupId>commons-io</groupId>
                <artifactId>commons-io</artifactId>
                <version>${commons-io.version}</version>
            </dependency>
            <dependency>
                <groupId>io.springfox</groupId>
                <artifactId>springfox-swagger2</artifactId>
                <version>${springfox-swagger2.version}</version>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <dependencies>

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-security</artifactId>
        </dependency>

		<dependency>
		    <groupId>org.springframework.security</groupId>
		    <artifactId>spring-security-config</artifactId>
		</dependency>

        <dependency>
            <groupId>org.springframework.data</groupId>
            <artifactId>spring-data-jpa</artifactId>
        </dependency>

        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
        </dependency>

        <dependency>
            <groupId>commons-io</groupId>
            <artifactId>commons-io</artifactId>
        </dependency>

        <dependency>
            <groupId>com.amazonaws</groupId>
            <artifactId>aws-java-sdk-ssm</artifactId>
        </dependency>

        <dependency>
            <groupId>io.swagger</groupId>
            <artifactId>swagger-annotations</artifactId>
            <version>${swagger-annotations.version}</version>
        </dependency>

        <dependency>
            <groupId>io.springfox</groupId>
            <artifactId>springfox-swagger2</artifactId>
        </dependency>

        <!-- Tests -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
		    <groupId>io.projectreactor</groupId>
		    <artifactId>reactor-test</artifactId>
		    <version>3.1.0.RELEASE</version>
		    <scope>test</scope>
		</dependency>
    </dependencies>

    <build>
        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>org.springframework.boot</groupId>
                    <artifactId>spring-boot-maven-plugin</artifactId>
                    <executions>
                        <execution>
                            <goals>
                                <goal>build-info</goal>
                            </goals>
                        </execution>
                    </executions>
                </plugin>
                <plugin>
                    <groupId>io.swagger</groupId>
                    <artifactId>swagger-codegen-maven-plugin</artifactId>
                    <version>${swagger-codegen-maven-plugin.version}</version>
                    <executions>
                        <execution>
                            <id>generate-sources-api</id>
                            <phase>generate-sources</phase>
                            <goals>
                                <goal>generate</goal>
                            </goals>
                            <configuration>
                                <inputSpec>${project.basedir}/src/main/resources/swagger/api.yml</inputSpec>
                                <language>spring</language>
                                <addCompileSourceRoot>false</addCompileSourceRoot>
                                <generateSupportingFiles>false</generateSupportingFiles>
                                <output>${project.build.directory}/generated/swagger</output>
                                <modelPackage>com.framework.api.model</modelPackage>
                                <apiPackage>com.framework.api.controller</apiPackage>
                                <generateModelDocumentation>false</generateModelDocumentation>
                                <generateApiDocumentation>false</generateApiDocumentation>
                                <generateApiTests>false</generateApiTests>
                                <configOptions>
                                    <serializableModel>true</serializableModel>
                                    <dateLibrary>java8-localdatetime</dateLibrary>
                                    <java8>true</java8>
                                    <interfaceOnly>true</interfaceOnly>
                                </configOptions>
                            </configuration>
                        </execution>
                    </executions>
                </plugin>
                <plugin>
                    <groupId>org.codehaus.mojo</groupId>
                    <artifactId>build-helper-maven-plugin</artifactId>
                    <executions>
                        <execution>
                            <id>add-source-swagger</id>
                            <phase>generate-sources</phase>
                            <goals>
                                <goal>add-source</goal>
                            </goals>
                            <configuration>
                                <sources>
                                    <source>${project.build.directory}/generated/swagger/src/main/java</source>
                                </sources>
                            </configuration>
                        </execution>
                    </executions>
                </plugin>
            </plugins>
        </pluginManagement>
    </build>

</project>