#!/usr/bin/env bash


mvn clean install

ECR_URL=032481722713.dkr.ecr.us-east-1.amazonaws.com
ECR_REPO_NAME=ms-config
GIT_HASH=$(git rev-parse --short HEAD)
ECR_TAGS=$(aws ecr describe-images --repository-name=$ECR_REPO_NAME --image-ids=imageTag=$GIT_HASH 2> /dev/null)
CURRENT_DIR=$(pwd)

# error out if any command fail
set -e

# login to aws ecr
$(aws ecr get-login --region us-east-1 --no-include-email)

# avoid building the image from the same commit twice
if [ -z "$ECR_TAGS" ]; then
  docker build -t $ECR_URL/$ECR_REPO_NAME:latest -t $ECR_URL/$ECR_REPO_NAME:$GIT_HASH .
  docker push $ECR_URL/$ECR_REPO_NAME:$GIT_HASH
  docker push $ECR_URL/$ECR_REPO_NAME:latest
fi


cd ./terraform
terraform taint module.service.aws_ecs_task_definition.this
terraform plan -out output.json
terraform apply output.json

cd $CURRENT_DIR
