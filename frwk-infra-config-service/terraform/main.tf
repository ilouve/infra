terraform {
  backend "s3" {
    bucket = "terraform.frwk.com.br"
    key    = "vpc/frwk/ecs/config"
    region = "us-east-1"
  }
}

provider "aws" {
  version = "~> 2.0"
  region  = "us-east-1"
}

data "terraform_remote_state" "vpc" {
  backend = "s3"
  config  = {
    bucket = "terraform.frwk.com.br"
    key    = "vpc/frwk"
    region = "us-east-1"
  }
}
