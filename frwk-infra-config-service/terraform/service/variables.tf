variable "alb" {
  description = "The alb to attach this ECS"
  default = null
}

variable "protocol" {
  description = "The protocol to attach to this ECS"
  type = string
  default = "http"
}

variable "replicas" {
  description = "How many ECS clusters to run"
  type = number
  default = 1
}

variable "health_check" {
  description = "The application path to do a periodic healthcheck"
  default = {}
}

variable "host" {
  description = "The host to serve this application"
  type = string
  default = ""
}

variable "cluster" {
  description = "The cluster to attach this application"
}

variable "name" {
  description = "The task name"
}

variable "task_definitions" {
  description = "The tasks definitions for this ECS"
  type = list(object({
    json = string
  }))
}