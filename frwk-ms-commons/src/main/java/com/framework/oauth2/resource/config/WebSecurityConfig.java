package com.framework.oauth2.resource.config;

import org.springframework.boot.actuate.autoconfigure.security.servlet.EndpointRequest;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

//@Configuration
//@EnableGlobalMethodSecurity(prePostEnabled = true, proxyTargetClass = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    public void configure(final WebSecurity web) throws Exception {
        // Desabilita qualquer verificação de segurança para das URLs da documentação da API e do Spring Actuator.
    	System.out.println("Configurando filter!!");
        web.ignoring().antMatchers("/**").requestMatchers(EndpointRequest.toAnyEndpoint());
    }

}
