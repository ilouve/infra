package com.framework.security.annotation.processor;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.BeanCreationException;
import org.springframework.stereotype.Component;
import org.springframework.util.ReflectionUtils;

import com.amazonaws.services.simplesystemsmanagement.AWSSimpleSystemsManagementClientBuilder;
import com.amazonaws.services.simplesystemsmanagement.model.GetParametersRequest;
import com.amazonaws.services.simplesystemsmanagement.model.GetParametersResult;
import com.framework.common.annotation.processor.BaseBeanPostProcessor;
import com.framework.security.annotation.SecureParam;

@Component
public class SecureParamBeanPostProcessor extends BaseBeanPostProcessor {

    @Override
    protected Object doProcessBeforeInitialization(final Object bean, final String beanName) {
        return bean;
    }

    @Override
    protected Object doProcessAfterInitialization(final Object bean, final String beanName) {
        final Map<String, Field> paramsMapper = buildParamsByField(bean);

        if (!paramsMapper.isEmpty()) {
            final GetParametersRequest paramRequest = new GetParametersRequest()
                    .withWithDecryption(Boolean.valueOf(true)).withNames(paramsMapper.keySet());
            final GetParametersResult result = AWSSimpleSystemsManagementClientBuilder.defaultClient()
                    .getParameters(paramRequest);
            if (!result.getInvalidParameters().isEmpty()) {
                throw new BeanCreationException("Parameter not found: " + result.getInvalidParameters().toString());
            }

            setParametersValues(bean, result, paramsMapper);
        }

        return bean;
    }

    private Map<String, Field> buildParamsByField(final Object bean) {
        final Map<String, Field> paramsByField = new HashMap<>();

        ReflectionUtils.doWithFields(bean.getClass(), field -> {
            final SecureParam annotation = field.getDeclaredAnnotation(SecureParam.class);
            if (annotation != null) {
                try {
                    field.setAccessible(true);
                    paramsByField.put((String) field.get(bean), field);
                } catch (IllegalArgumentException | IllegalAccessException e) {
                    throw new BeanCreationException(e.getMessage());
                }
            }
        });

        return paramsByField;
    }

    private void setParametersValues(final Object bean, final GetParametersResult result,
            final Map<String, Field> paramsMapper) {
        result.getParameters().stream().forEach(parameter -> {
            final Field field = paramsMapper.get(parameter.getName());
            field.setAccessible(true);
            try {
                field.set(bean, parameter.getValue());
            } catch (final Exception e) {
                throw new BeanCreationException(e.getMessage());
            }
        });
    }

}
