package com.framework.util;

import java.util.Collection;
import java.util.Map;
import java.util.Objects;

public final class CollectionsUtil {

    private CollectionsUtil() {
    }

    public static boolean isNullOrEmpty(final Map<?, ?> map) {
        return Objects.isNull(map) || map.isEmpty();
    }

    public static boolean nonNullOrEmpty(final Map<?, ?> map) {
        return !isNullOrEmpty(map);
    }

    public static boolean isNullOrEmpty(final Collection<?> list) {
        return Objects.isNull(list) || list.isEmpty();
    }

    public static boolean nonNullOrEmpty(final Collection<?> list) {
        return !isNullOrEmpty(list);
    }

}
