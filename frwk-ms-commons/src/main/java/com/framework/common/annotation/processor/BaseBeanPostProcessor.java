package com.framework.common.annotation.processor;

import org.springframework.beans.factory.config.BeanPostProcessor;

public abstract class BaseBeanPostProcessor implements BeanPostProcessor {

    public static final String BASE_PACKAGE = "com.framework";

    protected abstract Object doProcessBeforeInitialization(Object paramObject, String paramString);

    protected abstract Object doProcessAfterInitialization(Object paramObject, String paramString);

    @Override
    public Object postProcessBeforeInitialization(final Object bean, final String beanName) {
        if (bean.getClass().getName().startsWith(BASE_PACKAGE)) {
            doProcessBeforeInitialization(bean, beanName);
        }
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(final Object bean, final String beanName) {
        if (bean.getClass().getName().startsWith(BASE_PACKAGE)) {
            doProcessAfterInitialization(bean, beanName);
        }
        return bean;
    }

}
