
package com.framework.ms.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ErrorDetails {

    private static String jbossServerName = System.getProperty("jboss.server.name");

    private String date;
    private String message;
    private String details;
    private String serverName;

    public ErrorDetails(String date, String message, String details) {
        this.date = date;
        this.message = message;
        this.details = details;

        this.serverName = jbossServerName;
    }

}
