package com.framework.ms.handler;

import java.lang.reflect.InvocationTargetException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolationException;
import javax.validation.Path;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.WordUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.BindException;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import com.framework.ms.exception.BaseException;
import com.framework.ms.model.ErrorDetails;

import io.swagger.annotations.ResponseHeader;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@ControllerAdvice
public class BaseExceptionHandler {

    private static final String TEMPLATE = "'%s' %s";
    private static final String DELIMITER = ", ";
    private static final String VIOLACAO = "Violação";
    private static final String VIOLACOES = "Violações";
    private static final String MESSAGE = "%s: %s.";
    private final SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

    @ExceptionHandler(HttpMediaTypeNotSupportedException.class)
    public final ResponseEntity<ErrorDetails> handle(HttpMediaTypeNotSupportedException ex, WebRequest request) {
        final ErrorDetails errorDetails = new ErrorDetails(fmt.format(new Date()), ex.getMessage(), request.getDescription(false));
        log.error(ex.getMessage());
        return new ResponseEntity<>(errorDetails, HttpStatus.UNSUPPORTED_MEDIA_TYPE);
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    public final ResponseEntity<ErrorDetails> handle(HttpMessageNotReadableException ex, WebRequest request) {
        final ErrorDetails errorDetails = new ErrorDetails(fmt.format(new Date()), ex.getMessage(), request.getDescription(false));
        log.error(ex.getMessage());
        return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public final ResponseEntity<ErrorDetails> handle(HttpRequestMethodNotSupportedException ex, WebRequest request) {
        final ErrorDetails errorDetails = new ErrorDetails(fmt.format(new Date()), ex.getMessage(), request.getDescription(false));
        log.error(ex.getMessage());
        return new ResponseEntity<>(errorDetails, HttpStatus.METHOD_NOT_ALLOWED);
    }

    @ExceptionHandler(BaseException.class)
    public final ResponseEntity<ErrorDetails> handle(BaseException ex, WebRequest request) {
        final ErrorDetails errorDetails = new ErrorDetails(fmt.format(new Date()), ex.getMessage(), request.getDescription(false));
        log.error(ex.getMessage());
        return new ResponseEntity<>(errorDetails, ex.getHttpStatus());
    }

    @ExceptionHandler(Exception.class)
    public final ResponseEntity<ErrorDetails> handle(Exception ex, WebRequest request) throws Exception {
        if (ex.getClass().getAnnotation(ResponseStatus.class) != null && ex.getClass().getAnnotation(ResponseStatus.class).code() != null) {
            throw ex;
        }

        final ErrorDetails errorDetails = new ErrorDetails(fmt.format(new Date()), ex.getMessage(), request.getDescription(false));
        log.error("Falha ao chamar o serviço." + errorDetails, ex);

        return new ResponseEntity<>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler
    @ResponseBody
    @ResponseHeader
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public Map<String, Object> handle(AccessDeniedException exception) {
        return error(exception.getMessage());
    }

    @ExceptionHandler
    @ResponseBody
    @ResponseHeader
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Map<String, Object> handle(MethodArgumentNotValidException exception) {
        return error(String.format(MESSAGE, exception.getBindingResult().getFieldErrors().size() > 1 ? VIOLACOES : VIOLACAO, exception.getBindingResult().getFieldErrors().stream()
                .map(item -> item == null ? "" : String.format(TEMPLATE, item.getField(), item.getDefaultMessage())).collect(Collectors.joining(DELIMITER))));
    }

    @ExceptionHandler
    @ResponseBody
    @ResponseHeader
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Map<String, Object> handle(ConstraintViolationException exception) {
        return error(String.format(MESSAGE, exception.getConstraintViolations().size() > 1 ? VIOLACOES : VIOLACAO, exception.getConstraintViolations().stream()
                .map(item -> item == null ? "" : String.format(TEMPLATE, parameterName(item.getPropertyPath()), item.getMessage())).collect(Collectors.joining(DELIMITER))));
    }

    @ExceptionHandler
    @ResponseBody
    @ResponseHeader
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Map<String, Object> handle(BindException exception) {
        return error(String.format(MESSAGE, exception.getAllErrors().size() > 1 ? VIOLACOES : VIOLACAO,
                exception.getAllErrors().stream()
                        .map(item -> item == null ? "" : String.format("'%s.%s' %s", WordUtils.capitalize(item.getObjectName()), fieldName(item), item.getDefaultMessage()))
                        .collect(Collectors.joining(DELIMITER))));
    }

    @ExceptionHandler
    @ResponseBody
    @ResponseHeader
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Map<String, Object> handle(MissingServletRequestParameterException exception) {
        return error(String.format("O parâmetro '%s (tipo: %s)' é obrigatório.", exception.getParameterName(), exception.getParameterType()));
    }

    @ExceptionHandler
    @ResponseBody
    @ResponseHeader
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Map<String, Object> handle(MethodArgumentTypeMismatchException exception) {
        return error(String.format("O tipo do parâmetro '%s (tipo: %s)' está incorreto.", exception.getName(), exception.getRequiredType()));
    }

    private Map<String, Object> error(Object message) {
        return Collections.singletonMap("message", message);
    }

    private String parameterName(Path path) {
        final String[] parts = path.toString().split("\\.");
        return parts[parts.length - 1];
    }

    private String fieldName(final ObjectError item) {
        try {
            return (String) PropertyUtils.getProperty(item, "field");
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            return "";
        }
    }
}