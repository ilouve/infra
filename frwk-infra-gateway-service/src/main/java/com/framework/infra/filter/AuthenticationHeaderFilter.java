package com.framework.infra.filter;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Component
public class AuthenticationHeaderFilter extends AbstractGatewayFilterFactory<AuthenticationHeaderFilter.Config> {

    private static final String NO_AUTHORIZATION_HEADER = "No authorization header.";
    private static final String OAUTH = "oauth";
    private List<String> publicPaths = Arrays.asList(
            "oauth",
            "/swagger-resources",
            "/swagger-ui.html",
            "/webjars",
            "swagger.index",
            "swagger.json",
            "/health",
            "/partners/me",
            "/me",
            "/auth",
            "/auth/refresh",
            "/auth/me",
            "/user"
    );
    private static final String JSON_MESSAGE_SUFFIX = "\"}";
    private static final String JSON_MESSAGE_PREFIX = "{\"message\": \"";
    private static final String HEADER_API_TOKEN = "Authorization";
    private static final String APPLICATION_JSON = "application/json";
    private static final String CONTENT_TYPE = "Content-Type";

    public AuthenticationHeaderFilter() {
        super(Config.class);
    }

    @Override
    public GatewayFilter apply(final Config config) {
//    	Authentication authentication = new UsernamePasswordAuthenticationToken("123", null, Collections.emptyList());
//    	SecurityContextHolder.getContext().setAuthentication(authentication);
        return (exchange, chain) -> {
            final ServerHttpRequest request = exchange.getRequest();
            System.out.println("#gateway " + request.getPath().value());

            if (!anyMatch(publicPaths, request.getPath().value())) {
            	System.out.println("#gateway não liberado publicamente");
                final Optional<String> headerApiToken = request.getHeaders().keySet().stream().filter(HEADER_API_TOKEN::equalsIgnoreCase).findFirst();

                if (!headerApiToken.isPresent()) {
                	System.out.println("#gateway não autenticado");
                    return error(exchange, NO_AUTHORIZATION_HEADER, HttpStatus.UNAUTHORIZED);
                }
                System.out.println("#gateway autenticado!");
                final String token = request.getHeaders().get(headerApiToken.get()).get(0);
                final ServerHttpRequest modifiedRequest = exchange.getRequest().mutate().header("Authorization", "Bearer " + token).build();
                return chain.filter(exchange.mutate().request(modifiedRequest).build());
            }
            System.out.println("#gateway liberado!");
            return chain.filter(exchange);
        };
    }

    private Mono<Void> error(final ServerWebExchange exchange, final String message, final HttpStatus status) {
    	System.out.println("Erro gateway!");
        exchange.getResponse().setStatusCode(status);
        exchange.getResponse().getHeaders().add(CONTENT_TYPE, APPLICATION_JSON);
        final DataBuffer buffer = exchange.getResponse().bufferFactory().wrap(jsonBytes(message));
        return exchange.getResponse().writeWith(Flux.just(buffer));
    }

    private byte[] jsonBytes(final String message) {
        return json(message).getBytes(StandardCharsets.UTF_8);
    }

    private String json(final String message) {
        return JSON_MESSAGE_PREFIX + message + JSON_MESSAGE_SUFFIX;
    }

    public static class Config extends AbstractGatewayFilterFactory.NameConfig {
    }
    
    private boolean anyMatch(List<String> paths, String fullPath) {
        return paths.stream().anyMatch(fullPath::contains);
    }

}
