//package com.framework.infra.service;
//
//import java.util.Calendar;
//import java.util.Collections;
//import java.util.Date;
//
//import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
//import org.springframework.security.core.Authentication;
//
//import com.framework.infra.exception.ForbiddenException;
//
//import io.jsonwebtoken.Jwts;
//import io.jsonwebtoken.SignatureAlgorithm;
//
//public class TokenAuthenticationService {
//
//	static final String SECRET = "psw#frameworkSwagger2017;";
//	static final String TOKEN_PREFIX = "Bearer";
//	static final String HEADER_STRING = "Authorization";
//	
//	public static String addAuthentication(String codUsuario) {
//		// Token expira em 5 minutos
//		Date date = recuperaDataExpiracao();
//		
//		String jwt = Jwts.builder()
//		.setSubject(codUsuario)
//		.setExpiration(date)
//		.signWith(SignatureAlgorithm.HS512, SECRET)
//		.compact();
//		
//		return jwt;
//	}
//	
//	public static Authentication getAuthentication(String token) {
//		try {
//			if (token != null) {
//				// faz parse do token
//				String user = Jwts.parser()
//								.setSigningKey(SECRET)
//								.parseClaimsJws(token.replace(TOKEN_PREFIX, ""))
//								.getBody()
//								.getSubject();
//				
//				if (user != null) {
//					return new UsernamePasswordAuthenticationToken(user, null, Collections.emptyList());
//				}
//			}
//		}catch(Exception e) {
//			throw new ForbiddenException();
//		}
//		return null;
//	}
//
//	private static Date recuperaDataExpiracao() {
//		Date date = new Date();
//		Calendar cal = Calendar.getInstance();
//		cal.setTime(date);
//		cal.add(Calendar.MINUTE, 5);
//		date = cal.getTime();
//		return date;
//	}
//	
//}
