package com.framework.infra.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import reactor.core.publisher.Mono;

@RestController
public class HystrixFallbackController {

    @GetMapping("/fallback")
    public Mono<String> fallback() {
        return Mono.just("System busy, please try later");
    }

}
